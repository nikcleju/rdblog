#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Nic'
SITENAME = u'My research blog'
SITEURL = ''

# doesn't work:
#THEME='./themes/pelican-bootstrap3'
# render_math doesn't work:
#THEME='./themes/foundation-default-colours'
#THEME='./themes/pelican-simplegrey'
#THEME='./themes/pelican-sober'
#THEME='./themes/blueidea'
THEME='./themes/gum'
THEME='./themes/elegant'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Bucharest'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#==========
# Plugins
#==========
PLUGIN_PATHS=['./plugins']
PLUGINS = ['render_math']
